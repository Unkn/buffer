/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <string.h>
#include <stdlib.h>

#include "buffer.h"

#define BUFFER_DEFAULT_CAPACITY 0x80

// TODO no c11 thread support in glibc yet
#if 0
#include <threads.h>
thread_local int buffer_error = BUFFER_SUCCESS;
#else
static int buffer_error = BUFFER_ERR_NONE;
#endif

buffer * buffer_new()
{
    return (buffer *)calloc(1, sizeof(buffer));
}

void buffer_free(buffer * buff)
{
    if (buff)
    {
        if (buff->data)
            free(buff->data);
        free(buff);
    }
}

uint8_t * buffer_release(buffer * buff)
{
    if (!buff)
        return 0;
    uint8_t * ret = buff->data;
    buff->data = 0;
    buff->capacity = 0;
    buff->size = 0;
    return ret;
}

buffer * buffer_copy(const buffer* buff)
{
    if (!buff)
        return 0;
    if (!buff->size)
        return buffer_new();
    buffer * ret = buffer_new();
    ret->data = (uint8_t *)malloc(buff->size);
    ret->capacity = buff->size;
    ret->size = buff->size;
    memcpy(ret->data, buff->data, ret->size);
    return ret;
}

void buffer_clear(buffer * buff)
{
    if (buff)
        buff->size = 0;
}

int buffer_write(buffer * buff, const void * data, size_t len)
{
    if (!buff || !data)
        return 0;
    if (!len)
        return 1;
    if (buff->capacity - buff->size < len && !buffer_expand(buff, len))
        return 0;
    memcpy(buff->data + buff->size, data, len);
    buff->size += len;
    return 1;
}

int buffer_expand(buffer * buff, size_t available)
{
    if (!buff)
        return 0;
    if (buff->size + available <= buff->capacity)
        return 1;

    size_t newCap = buff->capacity ? buff->capacity : BUFFER_DEFAULT_CAPACITY;
    while (newCap < buff->size + available)
    {
        // Use the typical doubling hueristic that results in amortized constant
        // time when copying is necessary
        size_t tmp = newCap << 1;
        // Check for integer overflow
        if (tmp < newCap)
            newCap = buff->size + available;
        else
            newCap = tmp;
    }

    // Realloc to extend when possible without having to create a new buffer
    // entirely and copy contents.
    uint8_t * tmp = (uint8_t *)realloc(buff->data, newCap);
    if (!tmp)
        return 0;
    buff->data = tmp;
    buff->capacity = newCap;
    return 1;
}
