/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <utility>

#include "buffer.hpp"
#include "buffer.h"

Buffer::Buffer() :
    buff(buffer_new(), buffer_free)
{
}

Buffer::Buffer(const Buffer & sb) :
    buff(nullptr, buffer_free)
{
    *this = sb;
}

Buffer::Buffer(Buffer && sb) :
    buff(nullptr, buffer_free)
{
    *this = std::move(sb);
}

Buffer::~Buffer()
{
}

Buffer & Buffer::operator=(const Buffer & sb)
{
    if (this != &sb)
        buff = std::unique_ptr<buffer, void(*)(buffer *)>(buffer_copy(sb.buff.get()), buffer_free);
    return *this;
}

Buffer & Buffer::operator=(Buffer && sb)
{
    if (this != &sb)
        buff = std::move(sb.buff);
    return *this;
}

uint8_t * Buffer::data()
{
    return buff->data;
}

const uint8_t * Buffer::data() const
{
    return buff->data;
}

size_t Buffer::size() const
{
    return buff->size;
}

size_t Buffer::capacity() const
{
    return buff->capacity;
}

uint8_t * Buffer::release()
{
    return buffer_release(buff.get());
}

void Buffer::clear()
{
    buffer_clear(buff.get());
}

#if 0
bool Buffer::size(size_t size) const
{
    if (buff->capacity < size)
        return false;
    buff->size = size;
    return true;
}
#endif

bool Buffer::expand(size_t avail)
{
    return buffer_expand(buff.get(), avail);
}

bool Buffer::resize(size_t newSize)
{
    bool ok = true;
    if (newSize > capacity())
        ok = buffer_expand(buff.get(), newSize);
    if (ok)
        buff->size = newSize;
    return ok;
}

bool Buffer::writeInternal(const uint8_t * data, size_t len)
{
    return buffer_write(buff.get(), data, len);
}
