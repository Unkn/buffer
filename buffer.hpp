/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <cstdint>
#include <memory>

struct buffer;

class Buffer
{
    public:
        Buffer();
        Buffer(const Buffer & sb);
        Buffer(Buffer && sb);
        ~Buffer();

        Buffer & operator=(const Buffer & sb);
        Buffer & operator=(Buffer && sb);

        template<typename T>
        bool write(const T & data);

        template<typename T>
        bool write(const T * data, size_t len);

        uint8_t * data();
        const uint8_t * data() const;
        size_t size() const;
        size_t capacity() const;

        uint8_t * release();

        void clear();

        //bool size(size_t size) const;
        bool expand(size_t avail);
        bool resize(size_t newSize);

    private:
        std::unique_ptr<buffer, void(*)(buffer *)> buff;

        bool writeInternal(const uint8_t * data, size_t len);
};

template<typename T>
bool Buffer::write(const T & data)
{
    return writeInternal(reinterpret_cast<const uint8_t *>(&data), sizeof(T));
}

template<typename T>
bool Buffer::write(const T * data, size_t len)
{
    return writeInternal(reinterpret_cast<const uint8_t *>(data), len * sizeof(T));
}

#endif // #ifndef _BUFFER_HPP
